# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

Gem::Specification.new do |s|
    s.name = "calculator"
    s.version = "0.0.2"
    s.date = "2015-01-01"
    s.summary = "A simple calculator"
    s.description = "A calculator that supports adding/subtracting/multiplying/dividing and multiple levels of parentheses"
    s.authors = ["Tom Norton"]
    s.email = "tomwnorton@gmail.com"
    s.files = Dir.glob("{bin,lib}/**/*")
    s.executables << "calculator"
    s.licenses = ["Mozilla Public License, v. 2.0"]
end