# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator.rb'

RSpec.describe :evaluate do
    it "supports adding" do
        expect(Calculator::evaluate("2 + 3")).to eq(5)
    end
    
    it "supports subtracting" do
        expect(Calculator::evaluate("10 - 6")).to eq(4)
    end
    
    it "supports multiplying" do
        expect(Calculator::evaluate("10 * 6")).to eq(60)
    end
    
    it "supports dividing" do
        expect(Calculator::evaluate("60 / 6")).to eq(10)
    end
    
    it "supports parentheses" do
        expect(Calculator::evaluate("(8)")).to eq(8)
    end
    
    it "supports chaining" do
        expect(Calculator::evaluate("(3 + 4) * 7 - 9")).to eq(40)
    end
    
    it "doesn't require whitespace" do
        expect(Calculator::evaluate("(3+4)*7-9")).to eq(40)
    end
    
    it "supports order of operations" do
        expect(Calculator::evaluate("3 + 4 * 7 - 9 / 3")).to eq(28)
    end
    
    it "supports multiple levels of parentheses" do
        expect(Calculator::evaluate("2 * (3 + 4 * (9 - 7))")).to eq(22)
    end
end