# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator/syntax/parser'

module Calculator
    module Syntax
        RSpec.describe Parser do
            before(:each) do
                @expressionFactory = double("expressionFactory")
            end
            
            it "parses addition" do
                lhs = double("lhs")
                add = double("add")
                rhs = double("rhs")
                allow(@expressionFactory).to receive(:create_number_expression).with(2).and_return(lhs)
                allow(@expressionFactory).to receive(:create_number_expression).with(3).and_return(rhs)
                allow(@expressionFactory).to receive(:create_add_expression).with(equal(lhs), equal(rhs)).and_return(add)
                
                firstNumberToken = double("firstNumberToken")
                plusToken = double("plusToken")
                secondNumberToken = double("secondNumberToken")
                parser = Parser.new(@expressionFactory, [firstNumberToken, plusToken, secondNumberToken])
                allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                allow(plusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_plus }
                allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(3) }
                
                result = parser.generate_expression
                expect(result).to equal(add)
            end
            
            it "parses subtraction" do
                lhs = double("lhs")
                subtract = double("subtract")
                rhs = double("rhs")
                allow(@expressionFactory).to receive(:create_number_expression).with(2).and_return(lhs)
                allow(@expressionFactory).to receive(:create_number_expression).with(3).and_return(rhs)
                allow(@expressionFactory).to receive(:create_subtract_expression).with(equal(lhs), equal(rhs)).and_return(subtract)
                
                firstNumberToken = double("firstNumberToken")
                minusToken = double("minusToken")
                secondNumberToken = double("secondNumberToken")
                parser = Parser.new(@expressionFactory, [firstNumberToken, minusToken, secondNumberToken])
                allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                allow(minusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_minus }
                allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(3) }
                
                result = parser.generate_expression
                expect(result).to equal(subtract)
            end
            
            it "parses multiplication" do
                lhs = double("lhs")
                multiply = double("multiply")
                rhs = double("rhs")
                allow(@expressionFactory).to receive(:create_number_expression).with(2).and_return(lhs)
                allow(@expressionFactory).to receive(:create_number_expression).with(3).and_return(rhs)
                allow(@expressionFactory).to receive(:create_multiply_expression).with(equal(lhs), equal(rhs)).and_return(multiply)
                allow(lhs).to receive(:update_structure_for_multiply) do |&block|
                    block.call(lhs)
                end
                
                firstNumberToken = double("firstNumberToken")
                multiplyToken = double("multiplyToken")
                secondNumberToken = double("secondNumberToken")
                parser = Parser.new(@expressionFactory, [firstNumberToken, multiplyToken, secondNumberToken])
                allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                allow(multiplyToken).to receive(:accept).with(parser) { |visitor| visitor.visit_multiply }
                allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(3) }
                
                result = parser.generate_expression
                expect(result).to equal(multiply)
            end
            
            it "parses division" do
                lhs = double("lhs")
                divide = double("divide")
                rhs = double("rhs")
                allow(@expressionFactory).to receive(:create_number_expression).with(12).and_return(lhs)
                allow(@expressionFactory).to receive(:create_number_expression).with(3).and_return(rhs)
                allow(@expressionFactory).to receive(:create_divide_expression).with(equal(lhs), equal(rhs)).and_return(divide)
                allow(lhs).to receive(:update_structure_for_divide) do |&block|
                    block.call(lhs)
                end
                
                firstNumberToken = double("firstNumberToken")
                divideToken = double("divideToken")
                secondNumberToken = double("secondNumberToken")
                parser = Parser.new(@expressionFactory, [firstNumberToken, divideToken, secondNumberToken])
                allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(12) }
                allow(divideToken).to receive(:accept).with(parser) { |visitor| visitor.visit_divide }
                allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(3) }
                
                result = parser.generate_expression
                expect(result).to equal(divide)
            end
            
            it "parses parentheses" do
                group = double("group")
                number = double("number")
                allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(number)
                allow(@expressionFactory).to receive(:create_group_expression).with(equal(number)).and_return(group)
                
                openParensToken = double("openParensToken")
                numberToken = double("numberToken")
                closingParensToken = double("closingParensToken")
                parser = Parser.new(@expressionFactory, [openParensToken, numberToken, closingParensToken])
                allow(openParensToken).to receive(:accept).with(parser) { |visitor| visitor.visit_opening_parenthesis }
                allow(numberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(1) }
                allow(closingParensToken).to receive(:accept).with(parser) { |visitor| visitor.visit_closing_parenthesis }
                
                result = parser.generate_expression
                expect(result).to equal(group)
            end
            
            context "when the previous item on the stack is a lower precedence" do
                it "parses multiplication before addition or subtraction" do
                    number = double("number")
                    add = double("add")
                    multiply = double("multiply")
                    
                    allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(number)
                    allow(@expressionFactory).to receive(:create_add_expression).with(any_args).and_return(add)
                    allow(@expressionFactory).to receive(:create_multiply_expression).with(any_args).and_return(multiply)
                    allow(add).to receive(:update_structure_for_multiply) do |&block|
                        block.call(nil)
                        nil
                    end
                    
                    firstNumberToken = double("firstNumberToken")
                    plusToken = double("plusToken")
                    secondNumberToken = double("secondNumberToken")
                    multiplyToken = double("multiplyToken")
                    thirdNumberToken = double("thirdNumberToken")
                    parser = Parser.new(@expressionFactory,
                                                    [firstNumberToken,
                                                     plusToken,
                                                     secondNumberToken,
                                                     multiplyToken,
                                                     thirdNumberToken])
                    allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(5) }
                    allow(plusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_plus }
                    allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                    allow(multiplyToken).to receive(:accept).with(parser) { |visitor| visitor.visit_multiply }
                    allow(thirdNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(3) }
                    
                    result = parser.generate_expression
                    expect(result).to equal(add)
                end
                
                it "parses division before addition or subtraction" do
                    number = double("number")
                    add = double("add")
                    divide = double("divide")
                    
                    allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(number)
                    allow(@expressionFactory).to receive(:create_add_expression).with(any_args).and_return(add)
                    allow(@expressionFactory).to receive(:create_divide_expression).with(any_args).and_return(divide)
                    allow(add).to receive(:update_structure_for_divide) do |&block|
                        block.call(nil)
                        nil
                    end
                    
                    firstNumberToken = double("firstNumberToken")
                    plusToken = double("plusToken")
                    secondNumberToken = double("secondNumberToken")
                    divideToken = double("divideToken")
                    thirdNumberToken = double("thirdNumberToken")
                    parser = Parser.new(@expressionFactory,
                                                    [firstNumberToken,
                                                     plusToken,
                                                     secondNumberToken,
                                                     divideToken,
                                                     thirdNumberToken])
                    allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(5) }
                    allow(plusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_plus }
                    allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(12) }
                    allow(divideToken).to receive(:accept).with(parser) { |visitor| visitor.visit_divide }
                    allow(thirdNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                    
                    result = parser.generate_expression
                    expect(result).to equal(add)
                end
            end
            
            context "when two or more numbers are not separated by binary operators" do
                it "raises an exception" do
                    firstNumber = double("firstNumber")
                    secondNumber = double("secondNumber")
                    allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(firstNumber, secondNumber)
                    
                    firstNumberToken = double("firstNumberToken")
                    secondNumberToken = double("secondNumberToken")
                    parser = Parser.new(@expressionFactory, [firstNumberToken, secondNumberToken])
                    allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(1) }
                    allow(secondNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(2) }
                    
                    expect{parser.generate_expression}.to raise_error("A syntax error was found")
                end
            end
            
            context "when a binary operator is not followed by a right-hand operand" do
                it "raises an exception" do
                    firstNumber = double("firstNumber")
                    add = double("add")
                    allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(firstNumber)
                    allow(@expressionFactory).to receive(:create_add_expression).with(any_args).and_return(add)
                    
                    firstNumberToken = double("firstNumberToken")
                    plusToken = double("plusToken")
                    parser = Parser.new(@expressionFactory, [firstNumberToken, plusToken])
                    allow(firstNumberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(1) }
                    allow(plusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_plus }
                    
                    expect{parser.generate_expression}.to raise_error("A binary operator must be followed by an operand")
                end
            end
            
            context "when a binary operator is not preceded by a left-hand operand" do
                it "raises an exception" do
                    add = double("add")
                    firstNumber = double("firstNumber")
                    allow(@expressionFactory).to receive(:create_add_expression).with(any_args).and_return(add)
                    allow(@expressionFactory).to receive(:create_number_expression).with(any_args).and_return(firstNumber)
                    
                    plusToken = double("plusToken")
                    numberToken = double("numberToken")
                    parser = Parser.new(@expressionFactory, [plusToken, numberToken])
                    allow(plusToken).to receive(:accept).with(parser) { |visitor| visitor.visit_plus }
                    allow(numberToken).to receive(:accept).with(parser) { |visitor| visitor.visit_number(1) }
                    
                    expect{parser.generate_expression}.to raise_error("A binary operator must be preceded by an operand")
                end
            end
            
            context "when an opening parenthesis is not closed" do
                it "raises an exception" do
                    parensToken = double("parensToken")
                    parser = Parser.new(@expressionFactory, [parensToken])
                    allow(parensToken).to receive(:accept).with(parser) { |visitor| visitor.visit_opening_parenthesis }
                    
                    expect{parser.generate_expression}.to raise_error("A parenthesis was not closed")
                end
            end
            
            context "when an closing parenthesis does not have a matching opening parenthesis" do
                it "raises an exception" do
                    parensToken = double("parensToken")
                    parser = Parser.new(@expressionFactory, [ClosingParensToken.new])
                    allow(parensToken).to receive(:accept).with(parser) { |visitor| visitor.visit_closing_parenthesis }
                    
                    expect{parser.generate_expression}.to raise_error("An closing parenthesis does not have a matching opening parenthesis")
                end
            end
        end
    end
end