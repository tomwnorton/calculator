require('calculator/syntax/tokens')

module Calculator
    module Syntax
        RSpec.shared_context :token_specs, :a => :b do
            before(:each) { @visitor = double("visitor") }
        end
        
        RSpec.describe NumberToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_number).with(7)
                NumberToken.new(7).accept(@visitor)
            end
        end
        
        RSpec.describe PlusToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_plus)
                PlusToken.new.accept(@visitor)
            end
        end
        
        RSpec.describe MinusToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_minus)
                MinusToken.new.accept(@visitor)
            end
        end
        
        RSpec.describe MultiplicationToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_multiply)
                MultiplicationToken.new.accept(@visitor)
            end
        end
        
        RSpec.describe DivisionToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_divide)
                DivisionToken.new.accept(@visitor)
            end
        end
        
        RSpec.describe OpeningParensToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_opening_parenthesis)
                OpeningParensToken.new.accept(@visitor)
            end
        end
        
        RSpec.describe ClosingParensToken do
            include_context :token_specs
            it "accepts the visitor" do
                expect(@visitor).to receive(:visit_closing_parenthesis)
                ClosingParensToken.new.accept(@visitor)
            end
        end
    end
end