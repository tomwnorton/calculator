# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator/semantics.rb'

module Calculator
    RSpec.describe NumberExpression do
        before(:each) do
            @expr = NumberExpression.new(5)
        end
        
        it "evaluates to the number" do
            expect(@expr.evaluate).to eq(5)
        end
        
        it "evaluates in a left-to-right order for multiplication" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@expr))
        end
        
        it "evaluates in a left-to-right order for division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@expr))
        end
    end

    RSpec.describe AddExpression do
        before(:each) do
            @lhs = double("lhs")
            @rhs = double("rhs")
            @expr = AddExpression.new(@lhs, @rhs)
        end
        
        it "evaluates to the sum of its operands" do
            allow(@lhs).to receive(:evaluate) { 5 }
            allow(@rhs).to receive(:evaluate) { 7 }
            expect(@expr.evaluate).to eq(12)
        end
        
        it "has a lower precedence than multiplication" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@rhs))
            
            result = @expr.update_structure_for_multiply { |arg| arg }
            expect(result).to be_nil
        end
        
        it "has a lower precedence than division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@rhs))
            
            result = @expr.update_structure_for_divide { |arg| arg }
            expect(result).to be_nil
        end
    end

    RSpec.describe SubtractExpression do
        before(:each) do
            @lhs = double("lhs")
            @rhs = double("rhs")
            @expr = SubtractExpression.new(@lhs, @rhs)
        end
        
        it "evaluates to the difference of its operands" do
            allow(@lhs).to receive(:evaluate) { 7 }
            allow(@rhs).to receive(:evaluate) { 5 }
            expect(@expr.evaluate).to eq(2)
        end
        
        it "has a lower precedence than multiplication" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@rhs))
            
            result = @expr.update_structure_for_multiply { |arg| arg }
            expect(result).to be_nil
        end
        
        it "has a lower precedence than division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@rhs))
            
            result = @expr.update_structure_for_divide { |arg| arg }
            expect(result).to be_nil
        end
    end

    RSpec.describe MultiplyExpression do
        before(:each) do
            @lhs = double("lhs")
            @rhs = double("rhs")
            @expr = MultiplyExpression.new(@lhs, @rhs)
        end
        
        it "evaluates to the difference of its operands" do
            allow(@lhs).to receive(:evaluate) { 7 }
            allow(@rhs).to receive(:evaluate) { 5 }
            expect(@expr.evaluate).to eq(35)
        end
        
        it "evalulates in a left-to-right order with multiplications" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_multiply { |arg| arg }
            expect(result).not_to be_nil
        end
        
        it "evalulates in a left-to-right order with division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_divide { |arg| arg }
            expect(result).not_to be_nil
        end
    end

    RSpec.describe DivideExpression do
        before(:each) do
            @lhs = double("lhs")
            @rhs = double("rhs")
            @expr = DivideExpression.new(@lhs, @rhs)
        end
        
        it "evaluates to the quotient of its operands" do
            allow(@lhs).to receive(:evaluate) { 12 }
            allow(@rhs).to receive(:evaluate) { 3 }
            expect(@expr.evaluate).to eq(4)
        end
        
        it "evalulates in a left-to-right order with multiplications" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_multiply { |arg| arg }
            expect(result).not_to be_nil
        end
        
        it "evalulates in a left-to-right order with division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_divide { |arg| arg }
            expect(result).not_to be_nil
        end
    end
    
    RSpec.describe GroupExpression do
        before(:each) do
            @operand = double("operand")
            @expr = GroupExpression.new(@operand)
        end
        
        it "evaluates to its operand" do
            allow(@operand).to receive(:evaluate) { 5 }
            expect(@expr.evaluate).to eq(5)
        end
        
        it "evaluates in a left-to-right order with multiplication" do
            expect { |block| @expr.update_structure_for_multiply(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_multiply { |arg| arg }
            expect(result).not_to be_nil
        end
        
        it "evaluates in a left-to-right order with division" do
            expect { |block| @expr.update_structure_for_divide(&block) }.to yield_with_args(equal(@expr))
            
            result = @expr.update_structure_for_divide { |arg| arg }
            expect(result).not_to be_nil
        end
    end
end