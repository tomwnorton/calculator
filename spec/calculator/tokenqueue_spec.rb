# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator/tokenqueue'

module Calculator
    RSpec.describe TokenQueue do
        before(:each) do
            @tokenFactory = double("tokenFactory")
            @visitor = spy("visitor")
        end
        
        it "contains a number token" do
            numberToken = double("numberToken")
            allow(@tokenFactory).to receive(:create_number_token).with(17.9).and_return(numberToken)
            tokens = TokenQueue.new(@tokenFactory, "17.9")
            expect(tokens.first).to equal(numberToken)
        end
        
        it "contains a plus sign token" do
            plusToken = double("plusToken")
            allow(@tokenFactory).to receive(:create_plus_token).and_return(plusToken)
            tokens = TokenQueue.new(@tokenFactory, "+")
            expect(tokens.first).to equal(plusToken)
        end

        it "contains a series of tokens that can be removed in order" do
            firstToken = double("firstToken")
            secondToken = double("secondToken")
            thirdToken = double("thirdToken")
            
            allow(@tokenFactory).to receive(:create_number_token).with(1).and_return(firstToken)
            allow(@tokenFactory).to receive(:create_plus_token).and_return(secondToken)
            allow(@tokenFactory).to receive(:create_number_token).with(24).and_return(thirdToken)
            
            tokens = TokenQueue.new(@tokenFactory, "1 + 24")
            tokensArray = Array.new
            while !tokens.empty?
                tokensArray << tokens.first
                tokens.shift
            end
            
            expect(tokensArray.size).to eq(3)
            expect(tokensArray[0]).to equal(firstToken)
            expect(tokensArray[1]).to equal(secondToken)
            expect(tokensArray[2]).to equal(thirdToken)
        end
        
        it "contains a minus sign token when" do
            minusToken = double("minusToken")
            allow(@tokenFactory).to receive(:create_minus_token).and_return(minusToken)
            tokens = TokenQueue.new(@tokenFactory, "-")
            expect(tokens.first).to equal(minusToken)
        end
        
        context "when a token is not valid" do
            it "raises an exception" do
                tokens = TokenQueue.new(@tokenFactory, "fake")
                expect{tokens.first}.to raise_error("'fake' starts with an invalid token")
            end
        end
        
        it "contains a multiplication sign token" do
            multiplyToken = double("multiplyToken")
            allow(@tokenFactory).to receive(:create_multiplication_token).and_return(multiplyToken)
            tokens = TokenQueue.new(@tokenFactory, "*")
            expect(tokens.first).to equal(multiplyToken)
        end
        
        it "contains a division sign token" do
            divisionToken = double("divisionToken")
            allow(@tokenFactory).to receive(:create_division_token).and_return(divisionToken)
            tokens = TokenQueue.new(@tokenFactory, "/")
            expect(tokens.first).to equal(divisionToken)
        end
        
        it "contains an opening parenthesis token" do
            parenToken = double("parenToken")
            allow(@tokenFactory).to receive(:create_opening_parenthesis_token).and_return(parenToken)
            tokens = TokenQueue.new(@tokenFactory, "(")
            expect(tokens.first).to equal(parenToken)
        end
        
        it "contains a closing parenthesis token" do
            parenToken = double("parenToken")
            allow(@tokenFactory).to receive(:create_closing_parenthesis_token).and_return(parenToken)
            tokens = TokenQueue.new(@tokenFactory, ")")
            expect(tokens.first).to equal(parenToken)
        end
        
        context "when a . is found without any digits" do
            it "raises an exception" do
                tokens = TokenQueue.new(@tokenFactory, ".")
                expect{tokens.first}.to raise_error("'.' starts with an invalid token")
            end
        end
        
        context "when it is empty and first is called" do
            it "raises an exception" do
                tokens = TokenQueue.new(@tokenFactory, "")
                expect{tokens.first}.to raise_error("tokens queue is empty")
            end
        end
        
        context "when it is empty and shift is called" do
            it "raises an exception" do
                tokens = TokenQueue.new(@tokenFactory, "")
                expect{tokens.shift}.to raise_error("tokens queue is empty")
            end
        end
    end
end