# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator/syntax/tokens'

module Calculator
    class PatternAction
        attr_reader :pattern, :create_token
        
        def initialize(pattern, create_token)
            @pattern = pattern
            @create_token = create_token
        end
    end
    
    class TokenQueue
        def initialize(tokenFactory, text)
            @text = text
            @index = 0
            
            @actions = Array.new
            @actions << PatternAction.new(%r{^\s*\+}, lambda { |part| tokenFactory.create_plus_token })
            @actions << PatternAction.new(%r{^\s*-}, lambda { |part| tokenFactory.create_minus_token })
            @actions << PatternAction.new(%r{^\s*\*}, lambda { |part| tokenFactory.create_multiplication_token })
            @actions << PatternAction.new(%r{^\s*/}, lambda { |part| tokenFactory.create_division_token })
            @actions << PatternAction.new(%r{^\s*\(}, lambda { |part| tokenFactory.create_opening_parenthesis_token })
            @actions << PatternAction.new(%r{^\s*\)}, lambda { |part| tokenFactory.create_closing_parenthesis_token })
            @actions << PatternAction.new(%r{^\s*-?\d+(\.\d+)?}, lambda { |part| tokenFactory.create_number_token(part.to_f) })
        end
        
        def first
            if (empty?)
                raise "tokens queue is empty"
            end
            
            token_factory = get_token_factory
            part = get_current_range.match(token_factory.pattern)[0]
            token_factory.create_token.call(part)
        end
        
        def get_token_factory
            range = get_current_range
            token_factory = @actions.find { |factory| range.match(factory.pattern) != nil }
            if (token_factory == nil)
                raise "'#{range}' starts with an invalid token"
            end
            token_factory
        end
        
        def get_current_range
            @text[@index .. @text.size - 1]
        end
        
        def empty?
            @index >= @text.size
        end
        
        def shift
            if (empty?)
                raise "tokens queue is empty"
            end
            
            token_factory = get_token_factory
            part = get_current_range.match(token_factory.pattern)[0]
            @index += part.size
        end
        
        private :get_token_factory, :get_current_range
    end
end