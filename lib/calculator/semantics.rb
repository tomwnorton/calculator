# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

module Calculator
    class ExpressionFactory
        def create_number_expression(number)
            NumberExpression.new(number)
        end
        
        def create_add_expression(lhs, rhs)
            AddExpression.new(lhs, rhs)
        end
        
        def create_subtract_expression(lhs, rhs)
            SubtractExpression.new(lhs, rhs)
        end
        
        def create_multiply_expression(lhs, rhs)
            MultiplyExpression.new(lhs, rhs)
        end
        
        def create_divide_expression(lhs, rhs)
            DivideExpression.new(lhs, rhs)
        end
        
        def create_group_expression(operand)
            GroupExpression.new(operand)
        end
    end
    
    class NumberExpression
        def initialize(number)
            @number = number
        end
        
        def evaluate
            @number
        end
        
        def update_structure_for_multiply
            yield(self)
        end
        
        def update_structure_for_divide
            yield(self)
        end
    end
    
    class AddExpression
        def initialize(lhs, rhs)
            @lhs = lhs
            @rhs = rhs
        end
        
        def evaluate
            @lhs.evaluate + @rhs.evaluate
        end
        
        def update_structure_for_multiply
            @rhs = yield(@rhs)
            nil
        end
        
        def update_structure_for_divide
            @rhs = yield(@rhs)
            nil
        end
    end
    
    class SubtractExpression
        def initialize(lhs, rhs)
            @lhs = lhs
            @rhs = rhs
        end
        
        def evaluate
            @lhs.evaluate - @rhs.evaluate
        end
        
        def update_structure_for_multiply
            @rhs = yield(@rhs)
            nil
        end
        
        def update_structure_for_divide
            @rhs = yield(@rhs)
            nil
        end
    end
    
    class MultiplyExpression
        def initialize(lhs, rhs)
            @lhs = lhs
            @rhs = rhs
        end
        
        def evaluate
            @lhs.evaluate * @rhs.evaluate
        end
        
        def update_structure_for_multiply
            yield(self)
        end
        
        def update_structure_for_divide
            yield(self)
        end
    end
    
    class DivideExpression
        def initialize(lhs, rhs)
            @lhs = lhs
            @rhs = rhs
        end
        
        def evaluate
            @lhs.evaluate / @rhs.evaluate
        end
        
        def update_structure_for_multiply
            yield(self)
        end
        
        def update_structure_for_divide
            yield(self)
        end
    end
    
    class GroupExpression
        def initialize(operand)
            @operand = operand
        end
        
        def evaluate
            @operand.evaluate
        end
        
        def update_structure_for_multiply
            yield(self)
        end
        
        def update_structure_for_divide
            yield(self)
        end
    end
end