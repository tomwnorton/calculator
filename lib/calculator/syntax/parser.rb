# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require 'calculator/semantics.rb'

module Calculator
    module Syntax
        class Parser
            def initialize(expressionFactory, tokens)
                @expressionFactory = expressionFactory
                @tokens = tokens
                @stack = Array.new
                @unmatchedParentheses = 0
            end
            
            def generate_expression
                while !@tokens.empty?
                    token = @tokens.first
                    @tokens.shift
                    
                    token.accept(self)
                end
                
                expr = @stack.pop
                if (!@stack.empty?)
                    raise "A syntax error was found"
                end
                
                expr
            end
            
            def visit_number(number)
                @stack.push @expressionFactory.create_number_expression(number)
            end
            
            def visit_plus
                visit_binary { |lhs, rhs| @stack.push @expressionFactory.create_add_expression(lhs, rhs) }
            end
            
            def visit_binary
                if (@tokens.empty?)
                    raise "A binary operator must be followed by an operand"
                end
                
                if (@stack.empty?)
                    raise "A binary operator must be preceded by an operand"
                end
                lhs = @stack.pop
                
                nextToken = @tokens.first
                @tokens.shift
                nextToken.accept(self)
                rhs = @stack.pop
                
                yield(lhs, rhs)
            end
            private :visit_binary
            
            def visit_minus
                visit_binary { |lhs, rhs| @stack.push @expressionFactory.create_subtract_expression(lhs, rhs) }
            end
            
            def visit_multiply
                update_structure do |lhs, rhs|
                    lhs.update_structure_for_multiply { |lhs| @expressionFactory.create_multiply_expression(lhs, rhs) }
                end
            end
            
            def update_structure
                visit_binary do |lhs, rhs|
                    result = yield(lhs, rhs)
                    if (result == nil)
                        @stack.push lhs
                    else
                        @stack.push result
                    end
                end
            end
            private :update_structure
            
            def visit_divide
                update_structure do |lhs, rhs|
                    lhs.update_structure_for_divide { |lhs| @expressionFactory.create_divide_expression(lhs, rhs) }
                end
            end
            
            def visit_opening_parenthesis
                unmatchedParentheses = @unmatchedParentheses
                @unmatchedParentheses += 1
                
                while (@unmatchedParentheses > unmatchedParentheses)
                    if (@tokens.empty?)
                        raise "A parenthesis was not closed"
                    end
                    
                    nextToken = @tokens.first
                    @tokens.shift
                    nextToken.accept(self)
                end
                
                expr = @stack.pop
                @stack.push(@expressionFactory.create_group_expression(expr))
            end
            
            def visit_closing_parenthesis
                if (@unmatchedParentheses == 0)
                    raise "An closing parenthesis does not have a matching opening parenthesis"
                end
                @unmatchedParentheses -= 1
            end
        end
    end
end