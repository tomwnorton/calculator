# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

module Calculator
    module Syntax
        class TokenFactory
            def create_number_token(number)
                NumberToken.new(number)
            end
            
            def create_plus_token
                PlusToken.new
            end
            
            def create_minus_token
                MinusToken.new
            end
            
            def create_multiplication_token
                MultiplicationToken.new
            end
            
            def create_division_token
                DivisionToken.new
            end
            
            def create_opening_parenthesis_token
                OpeningParensToken.new
            end
            
            def create_closing_parenthesis_token
                ClosingParensToken.new
            end
        end
        
        class NumberToken
            def initialize(number)
                @number = number
            end
            
            def accept(visitor)
                visitor.visit_number @number
            end
        end
        
        class PlusToken
            def accept(visitor)
                visitor.visit_plus
            end
        end
        
        class MinusToken
            def accept(visitor)
                visitor.visit_minus
            end
        end
        
        class MultiplicationToken
            def accept(visitor)
                visitor.visit_multiply
            end
        end
        
        class DivisionToken
            def accept(visitor)
                visitor.visit_divide
            end
        end
        
        class OpeningParensToken
            def accept(visitor)
                visitor.visit_opening_parenthesis
            end
        end
        
        class ClosingParensToken
            def accept(visitor)
                visitor.visit_closing_parenthesis
            end
        end
    end
end