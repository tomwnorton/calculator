# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

require("calculator/tokenqueue")
require("calculator/syntax/parser")

module Calculator
    def self.evaluate(text)
        tokenFactory = Syntax::TokenFactory.new
        tokens = TokenQueue.new(tokenFactory, text)
        expressionFactory = ExpressionFactory.new
        parser = Syntax::Parser.new(expressionFactory, tokens)
        
        expr = parser.generate_expression
        expr.evaluate
    end
end